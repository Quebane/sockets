all: file client server

file:
	gcc -fpic -fPIC -shared -o ./file.so FileIO/file.c FileIO/file.h

client:
	gcc client_socket/client.c client_socket/client.h client_socket/file.h ./file.so -o client.out

server:
	gcc -pthread server_socket/server.c server_socket/server.h server_socket/file.h ./file.so -o server.out