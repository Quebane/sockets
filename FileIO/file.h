#ifndef SOCKETS_FILE_H
#define SOCKETS_FILE_H
    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>

    #define BUFFER_SIZE 1024
    #define min(X,Y) ((X) < (Y) ? (X) : (Y))

    typedef struct file_struct file_struct;

    FILE* file;
    char buffer[BUFFER_SIZE];
    long long current_pointer;
    long long file_length;

    struct file_struct
    {
        char* buffer;
        int length;
    };
    void open_file(char* file_path, char* mode);
    void close_file();
    file_struct* read_file();
    void write_file(file_struct* buffer);
#endif //SOCKETS_FILE_H
