#include "file.h"

file_struct* read_file()
{
   memset((char*)&buffer, 0, BUFFER_SIZE);
   int min_length = min(file_length - current_pointer, BUFFER_SIZE);
   if(file_length - current_pointer > 0)
   {
      fread(buffer, 1, min_length, file);
      file_struct* read_file_struct = (file_struct*)malloc(sizeof(file_struct));
      read_file_struct->buffer = buffer;
      read_file_struct->length = min_length;
      current_pointer += min_length;
      return read_file_struct;
   }
   return NULL;
}

void write_file(file_struct* write_file_struct)
{
   fwrite(write_file_struct->buffer, write_file_struct->length, 1,  file);
}

void open_file(char* file_path, char* type)
{
   file = fopen(file_path, type);
   if(!file)
   {
      printf("%s", "Error with openingfile!");
      exit(0);
   }
   if(type[0] == 'r')
   {
      fseek (file , 0 , SEEK_END);
      file_length = ftell(file);
      rewind(file);
   }
   current_pointer = 0;
}

void close_file()
{
   fclose(file);
   current_pointer = 0;
}