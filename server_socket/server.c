/* A simple server in the internet domain using TCP
   The port number is passed as an argument */
#include "server.h"
#include "file.h"

void error(const char *msg)
{
    perror(msg);
    exit(1);
}

void* connection_handler(void *socket_descriptor)
{
    //Get the socket descriptor
    int socket = *(int*) socket_descriptor;
    int data_length;

    //Receive a message from client
    file_struct write_file_struct;
    int current = 0;
    open_file(write_file_path, "wb");
    printf("%s", "Starting read file");
    while( (data_length = recv(socket, server_buffer , BUFFER_SIZE , 0)) > 0 )
    {
        write_file_struct.buffer = server_buffer;
        write_file_struct.length = data_length;
        write_file(&write_file_struct);
        bzero(server_buffer, BUFFER_SIZE);
    }
    printf("%s", "File readed");
    close_file();
    if(data_length == 0)
    {
        puts("Client disconnected");
        fflush(stdout);
    }
    else if(data_length == -1)
    {
        perror("recv failed");
    }
    close(client_socket_descriptor);
    return 0;
}

void init_server(int argc, char *argv[])
{
     if (argc < 3) {
         fprintf(stderr,"ERROR, no port provided\n");
         exit(1);
     }
     write_file_path = argv[2];
     //The AF_INET address family is the address family for IPv4.
     //SOCK_STREAM - TCP type of socket
     //0 - default value for choosing protocol
     socket_descriptor = socket(AF_INET, SOCK_STREAM, 0);
     if (socket_descriptor < 0)
        error("ERROR opening socket");
     bzero((char *) &server_address, sizeof(server_address));
     port = atoi(argv[1]);
     server_address.sin_family = AF_INET;
     //Using local machine address
     server_address.sin_addr.s_addr = INADDR_ANY;
     //convert port to right network order bytes
     server_address.sin_port = htons(port);
     if (bind(socket_descriptor, (struct sockaddr *) &server_address,
              sizeof(server_address)) < 0)
              error("ERROR on binding");
}

int run_server()
{
    listen(socket_descriptor,5);
    client_address_length = sizeof(client_address);
    while(client_socket_descriptor = accept(socket_descriptor, (struct sockaddr *)&client_address,
                                             &client_address_length))
    {
        printf("%s\n", "New connection");
        if(pthread_create( &thread_id , NULL ,  connection_handler , (void*)&client_socket_descriptor) < 0)
        {
            perror("could not create thread");
            return 1;
        }
        pthread_join( thread_id , NULL);
    }
    close(socket_descriptor);
    return 0;
}

int main(int argc, char *argv[])
{
    init_server(argc, argv);
    run_server();
    return 0;
}