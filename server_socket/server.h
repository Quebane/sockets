#ifndef SERVER_SOCKET_SERVER_H
#define SERVER_SOCKET_SERVER_H
    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    #include <unistd.h>
    #include <sys/types.h>
    #include <sys/socket.h>
    #include <netinet/in.h>
    #include <pthread.h>
    #include "file.h"

    int socket_descriptor, client_socket_descriptor, port;
    socklen_t client_address_length;
    struct sockaddr_in server_address, client_address;
    int data_length;
    pthread_t thread_id;
    char server_buffer[BUFFER_SIZE];
    char* successful_message = (char*)"Everything is OK!";
    char* write_file_path;

    void error(const char *msg);
    void* connection_handler(void *socket_descriptor);
    void init_server(int argc, char *argv[]);
    int run_server();
#endif //SERVER_SOCKET_SERVER_H
