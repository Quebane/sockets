#ifndef SOCKETS_CLIENT_H
#define SOCKETS_CLIENT_H

    #include <stdio.h>
    #include <stdlib.h>
    #include <unistd.h>
    #include <string.h>
    #include <sys/types.h>
    #include <sys/socket.h>
    #include <netinet/in.h>
    #include <netdb.h>
    #include "file.h"
    #include <fcntl.h>

    #define CLIENT_BUFFER_SIZE 256

    int socket_descriptor, port, data_length;
    struct sockaddr_in server_address;
    struct hostent *server;
    char client_buffer[CLIENT_BUFFER_SIZE];

    void error(const char *msg);
    void init_access(char *argv[]);
    void send_file();
#endif //SOCKETS_CLIENT_H
