#include "client.h"

//function with error messages
void error(const char *msg)
{
    perror(msg);
    exit(0);
}

void init_access(char *argv[])
{
    port = atoi(argv[2]);
    //Non blocking mode
    socket_descriptor = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0);
    if (socket_descriptor < 0)
        error("ERROR opening socket");
    server = gethostbyname(argv[1]);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }
    bzero((char *) &server_address, sizeof(server_address));
    server_address.sin_family = AF_INET;
    bcopy((char *)server->h_addr,
         (char *)&server_address.sin_addr.s_addr,
         server->h_length);
    server_address.sin_port = htons(port);
    while (connect(socket_descriptor, (struct sockaddr *) &server_address, sizeof(server_address)) < 0) {}
}

void send_file(char* file_path)
{
    printf("%s\n", "Start sending file");
    open_file(file_path, "rb");
    file_struct* read_file_struct;
    while((read_file_struct = read_file()) != NULL)
    {
        data_length = write(socket_descriptor, read_file_struct->buffer, read_file_struct->length);
        if (data_length < 0)
            error("ERROR writing to socket");
    }
    printf("%s\n", "File send succesfully");
    close(socket_descriptor);
    close_file();
}

int main(int argc, char *argv[])
{
    if (argc < 4) {
       fprintf(stderr,"usage %s hostname port\n", argv[0]);
       exit(0);
    }
    init_access(argv);
    send_file(argv[3]);
    return 0;
}